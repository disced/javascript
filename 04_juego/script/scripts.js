function juega() {
    var frutas = [ "../img/orange.png", "../img/strawberry.png", "../img/watermelon.png" ],
        fruta1, fruta2, fruta3,
        aleatorio1, aleatorio2, aleatorio3,
        totalFrutas = frutas.length,
        totalAleatorio,
        resultado

    fruta1 = document.getElementById ( 'fruitOne'  )
    fruta2 = document.getElementById ( 'fruitTwo'  )
    fruta3 = document.getElementById ( 'fruitThree')

    // Lamada a la funcion 'frutaAleatoria'
    aleatorio1 = frutaAleatoria ( totalFrutas )
    aleatorio2 = frutaAleatoria ( totalFrutas )
    aleatorio3 = frutaAleatoria ( totalFrutas )

    // Cambio del atributo 'src' de las imagenes que se han guardado en fruta1, fruta2 y fruta3.
    fruta1.src = frutas[ aleatorio1 ]
    fruta2.src = frutas[ aleatorio2 ]
    fruta3.src = frutas[ aleatorio3 ]

    // Guardar en un array los valores generados aleatorios, en este caso del 0 al 2 inclusives.
    totalAleatorio = [ aleatorio1, aleatorio2, aleatorio3 ]

    // Comprobar si coinciden las frutas
    resultado = compruebaFrutas ( totalAleatorio )

    // Añade 500, 1000 o 0 puntos en funcion del resultado de compruebaFrutas().
    compruebaPuntos ( resultado )

    // Cambio el atributo 'alt' ya que no 'value' no he consiguido hacerlo funcionar.
    // Utilizo este valor para saber que fruta tiene cada 'caja' para hacer la comprobacion
    // en la funcion avanza().
    fruta1.alt = aleatorio1
    fruta2.alt = aleatorio2
    fruta3.alt = aleatorio3
}

function frutaAleatoria ( totalFr ) { return Math.floor (  Math.random()  * totalFr ) }

function guardaPuntuacion() {
    var actual,
        total

    // Guardar los dos marcadores para sumar el actual al total y dejar actual en 0
    actual = document.getElementById ( 'curScore' ).value
    total  = document.getElementById ( 'totScore' ).value

    actual = parseInt ( actual, 10 )
    total  = parseInt ( total,  10 )
    total += actual;

    // Modifica los valores de los inputs
    totScore.value     = total
    curScore.value     = 0
}

function compruebaFrutas( totalAl ) {
    var total = 0;
    // Si coinciden 2 frutas retorna 5 (3 * 2).
    // Si coinciden 3 frutas retorna 9 (3 * 3).
    for ( i=0; i<totalAl.length; i++ )
        for ( j=0; j<totalAl.length; j++ )
            if ( totalAl[i] == totalAl[j] )
                total++
    return total
}

function avanza( fruta ) {
    var imgFrutas = [ "../img/orange.png", "../img/strawberry.png", "../img/watermelon.png" ],
        frutas    = [document.getElementById('fruitOne'), document.getElementById('fruitTwo'), document.getElementById('fruitThree')],
        frutas2,
        resultado,
        siguiente

    if ( frutas[fruta].alt == 2 )
        siguiente = 0
    else
        siguiente = parseInt ( frutas[fruta].alt, 10) + 1

    frutas[fruta].src = imgFrutas[siguiente]
    frutas[fruta].alt = siguiente

    frutas2   = [ document.getElementById('fruitOne').alt, document.getElementById('fruitTwo').alt, document.getElementById('fruitThree').alt ]
    resultado = compruebaFrutas (frutas2)
    compruebaPuntos(resultado)
}

function compruebaPuntos( res ) {
    if ( res == 5 )
        curScore.value = 500
    else if ( res == 9 )
        curScore.value = 1000
    else
        curScore.value = 0
}

function reinicia() {
    // Restaura todo a 0.
    curScore.value = 0
    totScore.value = 0
    fruitOne.src   = '../img/orange.png'
    fruitTwo.src   = '../img/strawberry.png'
    fruitThree.src = '../img/watermelon.png'
}

function diaActual() {
    // Imprime el dia actual y la hora en formato largo.
    var fechaLarga = new Date(),    // Fecha actual en formato largo.
        fechaWeb   = document.getElementById ('fecha')
    dias  = [ "Lunes", "Martes","Miercoles","Jueves","Viernes","Sabado","Domingo" ],

        meses = [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ]

    fechaWeb.innerHTML  = dias[ fechaLarga.getDay() - 1 ] + " "      // getDay devuelve el dia de la semana en un numero.
    fechaWeb.innerHTML += fechaLarga.getDate() + " de "              // getDate devuelve el dia del mes.
    fechaWeb.innerHTML += meses[ fechaLarga.getMonth() ] + " del "   // getMonth devuelve el mes, contando desde el 0 al 11, por eso no se resta.
    fechaWeb.innerHTML += fechaLarga.getFullYear() + " a las "       // getFullYear devuelve el año en formato largo 'yyyy'
    fechaWeb.innerHTML += fechaLarga.getHours() + ":" +  fechaLarga.getMinutes()
    //                        Devuelve la hora            Devuelve los minutos
}

function enviaMail() {
    window.open( 'malito:dragosdisce@gmail.com' )
}
