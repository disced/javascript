function contarDigitos() {
    var uNum,
        temp = 1,
        length = 0,
        out,
        digit,
        si_alert,   // success inicio alert
        sf_alert,   // success fin alert
        di_alert,   // danger inicio alert
        df_alert    // danger fin alert


    // Guardar numero del usuario
    uNum = document.getElementById ( 'userNum' )
    out  = document.getElementById ( 'digits'  )

    // Conversion a entero decimal
    uNum = parseInt ( uNum.value, 10 )

    // Cuenta digitos
    while ( temp <= uNum )
        length++, temp *= 10

    // Guarda 'digito' o 'digitos'
    if ( length == 1 )
        digit = "digito"
    else digit = "digitos"

    // Alerta success "verde"
    si_alert = '<div class="alert alert-success">'
    sf_alert = '</div>'

    // Alerta danger "roja"
    di_alert = '<div class="alert alert-danger">'
    df_alert = '</div>'

    // Inserta en el html en función de si no es numero
    // o si lo es

    if ( isNaN (uNum) )
        out.innerHTML = di_alert  + "No es un numero!" + df_alert
    else out.innerHTML = si_alert + uNum + " tiene " + length + " " + digit + sf_alert
}
