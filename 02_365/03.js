function calculaMayor() {
    var y_1, m_1, d_1,
        y_2, m_2, d_2,
        mayor,
        esn = "<br/>"

    // Año Uno
    y_1 = document.getElementById ( 'year_1' )
    m_1 = document.getElementById ( 'month_1' )
    d_1 = document.getElementById ( 'day_1' )

    // Año Dos
    y_2 = document.getElementById ( 'year_2' )
    m_2 = document.getElementById ( 'month_2' )
    d_2 = document.getElementById ( 'day_2' )

    // Conversion a enteros
    y_1 = parseInt( y_1.value, 10 )
    m_1 = parseInt( m_1.value, 10 )
    d_1 = parseInt( d_1.value, 10 )
    y_2 = parseInt( y_2.value, 10 )
    d_2 = parseInt( d_2.value, 10 )
    m_2 = parseInt( m_2.value, 10 )

    // Verifica año mayor
    if ( y_1 > y_2 )
        mayor = 1
    else
        mayor = 2
    if ( y_1 == y_2 ) {
        if ( m_1 > m_2 )
            mayor = 1
        else mayor = 2
        if ( m_1 == m_2 )
            if( d_1 > d_2 )
                mayor = 1
        else mayor = 2
    }
    if ( y_1 == y_2 && m_1 == m_2 && d_1 == d_2 )
        mayor = 0


    // Imprime mayor
    if ( mayor == 1 )
    {
        out.innerHTML = "El año mayor es el uno" + esn
        out.innerHTML += d_1 + " de " + retornaMes( m_1 ) + " del " + y_1
    }
    else if ( mayor == 2 )
    {
        out.innerHTML = "El año mayor es el dos" + esn
        out.innerHTML += d_2 + " de " + retornaMes( m_2 ) + " del " + y_2
    }
    else if ( mayor == 0 )
        out.innerHTML = "Son años iguales"

}

function retornaMes ( mes ) {
    var meses =
        [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ]
    return meses [mes - 1]
}
