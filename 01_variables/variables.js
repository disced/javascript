function BotonUno() {
    var parrafo,
        numero_uno=10,
        numero_dos=20,
        numero_tres=30;

    parrafo = document.getElementById('p1');
    parrafo.innerHTML = "Suma y Media </br>";
    parrafo.innerHTML += (numero_uno + numero_dos + numero_tres) + "</br>";
    parrafo.innerHTML += (numero_uno + numero_dos + numero_tres) / 3;
}


function BotonDos() {
    var parrafo,
        saludo = "Buenas Tardes!",
        despedida = "Hasta mañana";

    parrafo = document.getElementById('p2');
    parrafo.innerHTML = "Unión de cadenas </br>";
    parrafo.innerHTML += despedida + " - " + saludo;

}

function BotonTres() {
    var parrafo,
        logico = true;

    parrafo = document.getElementById('p3');
    parrafo.innerHTML = "Variable Logica </br>";
    parrafo.innerHTML += logico;
}
