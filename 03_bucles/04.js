/*
Web que lea un número y escriba si el número es capicua o no
*/
function numeroCapicua() {
	var num,		// num original
		buff,		// igual que num, lo uso para modificar su valor
		digit,	
		deci 	  = 1,
		num_reves = 0,
		count     = 0,
		capi 	  = " Es capicua"
		nocapi    = " No es capicua"

	num = document.getElementById( 'num' )
	num = parseInt ( num.value, 10 )
	
	// Calcula las cifras del numero
    buff = num
	while ( buff > 0 ) {       
        buff /= 10
		if ( buff < 1 )
			buff = 0
		deci *= 10
		count++		
	}

	deci /= 10		// Se divide ya que si tenemos el 123 'deci' vale 1000
    buff = num 		// Se vuelve a asignar su valor original

	// Guarda en num_reves el orginal al revés
	for ( i=0; i<count; i++ ) {
		digit = buff % 10                           // digit guarda el ultimo digito de buff
		buff /= 10                                  // elimina el ultimo digito de buff
		num_reves += deci * parseInt( digit, 10 )   // buff=123; digit=3; deci=100; 100*3 = 300...
		deci /= 10
	}

	// Si el numero original al reves = al original = es capicua
	out.innerHTML = num + "<br>"
	num_reves == num ? out.innerHTML += capi : out.innerHTML += nocapi
}