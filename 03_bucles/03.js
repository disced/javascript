/*
Web que lea un número y genere el número inverso del número dado.
Ej: Para número=2346, el inverso será: 6432 
*/
function escribeCifras() {
	var num,		// num original
		buff,		// igual que num, lo uso para modificar su valor
		digit,	
		deci 	  = 1,
		num_final = 0,
		count     = 0

	num = document.getElementById( 'num' )
	num = parseInt ( num.value, 10 )
	
	// Calcula las cifras del numero
    buff = num
	while ( buff > 0 ) {       
        buff /= 10
		if ( buff < 1 )
			buff = 0
		deci *= 10
		count++		
	}

	deci /= 10     // Se divide ya que si tenemos el 123 'deci' vale 1000
    buff = num     // Se vuelve a asignar su valor original

	// Guarda en num_final el orginal al revés
	for ( i=0; i<count; i++ ) {
		digit = buff % 10                           // digit guarda el ultimo digito de buff
		buff /= 10                                  // elimina el ultimo digito de buff
		num_final += deci * parseInt( digit, 10 )   // buff=123; digit=3; deci=100; 100*3 = 300...
		deci /= 10
	}
    out.innerHTML  = "Original <br>"  + num
	out.innerHTML += "<br><br>Del revés <br>" + num_final
}