function DiaSiguiente() {
    var year, month, day,
        n_year, n_month, n_day,
        n_dias, bis

    // Almacenando valores del usuario.
    year  = document.getElementById  ( 'year_id'  )
    month = document.getElementById  ( 'month_id' )
    day   = document.getElementById  ( 'day_id'   )


    // Conversion del valor de la variable a entero
    // decimal (10) y se guarda en la misma variable.
    year  = parseInt  ( year.value, 10  )
    month = parseInt  ( month.value, 10 )
    day   = parseInt  ( day.value, 10   )

    // Si modificase los valores de las varibles introducidas
    // por el usuario siempre se usaria el valor anterior modificado.
    n_year  = year
    n_month = month
    n_day   = day
    bis     = bisiesto ( n_year )

    meses_31 = [ 1, 3, 5, 7, 8, 10, 12 ]
    meses_30 = [ 4, 6, 9, 11 ]
    dias     = [ 31, 30, 29, 28 ]

    for ( i=0; i<meses_31.length; i++ ) {
        month == meses_31[i] ? n_dias = 31 : n_dias = 0
        if ( n_dias == 31 )
            break;
    }

    for ( i=0; i<meses_30.length; i++ ) {
        month == meses_30[i] ? n_dias = 30 : n_dias = 31
        if ( n_dias == 30 )
            break;
    }

    if ( month == 2 && bis )
        n_dias = 29
    else if ( month == 2 )
        n_dias = 28

    day == n_dias ? n_day = 1

/*

    if ( month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12 ) {

        // Si el dia es < 31 se le suma 1.
        if ( day < 31 )
            n_day += 1

        // Si es 31 de Diciembre se suma 1 al año y se deja en 1 el dia y el mes.
        else if ( month == 12 && day == 31 )
            n_year += 1, n_month = 1, n_day = 1

        // Si no es nigun caso anterior se suma 1 a dia y mes.
        else n_day = 1, n_month += 1

    }

    // Comprueba meses con 30 dias.
    if ( month == 4 || month == 6 || month == 9 || month == 11 ) {
        // Si el dia es < 30 se le suma 1.
        if ( day < 30 )
            n_day += 1
        // Si no se deja el dia en 1 y se suma 1 al mes
        else n_day = 1, n_month += 1
    }

    // Comprobamos Febrero
    if ( month == 2 ) {
        if ( bis == true && day == 29 )
            n_month += 1, n_day = 1
        else if ( bis == true && day < 29 )
            n_day += 1
        else if ( bis == false && day == 28 )
            n_month +=1, n_day = 1
        else if ( bis == false && day < 28 )
            n_day += 1
    }
*/
    // Imprime si un año es bisiesto o no.
    if ( bis == true )
        next.innerHTML = "Año Bisiesto <br>"
    else next.innerHTML = "Año No Bisiesto <br>"

    /*
     Imprime por ej:
     Año: 2020
     Mes: 3
     Dia: 1
     */
    next.innerHTML += "Año: " + n_year + "<br>" + "Mes: " + n_month + "<br>" + "Dia: " + n_day + "<br>"

    /*
     *
     * Dudo si debo usar next.innerHTML o el id del html
     * diaSiguiente.innerHTML, funciona con las dos,
     * imagino que 'next' al contener el elemento 'diaSiguiente'
     * se puede hacer de las dos formas.
     *
     */
}

function bisiesto ( b_year ) {
    if ( ( b_year % 4 ) == 0 && ( b_year % 100 ) != 0 || ( b_year % 400 ) == 0 )
        return true
    else return false

    // bisiesto = 1 / true
    // no bisiesto = 0 / false
}