/*
Web que lea un número y escriba en cada línea las cifras del número. 
Ej: Numero = 2765 , que escriba las cifras 2, 7, 6, y 5 
en distintas líneas cada una.
*/

function escribeCifras() {
	var num,
	    count = 0,
	    digitos = []

	num = document.getElementById( 'num' )
	num = parseInt ( num.value, 10 )

	// Calculos
	while ( num > 0 ) {
        res = Math.trunc( num % 10 )
        num /= 10
        
        digitos.push( res ) // digitos[0] = num, digitos[1] = num.....digitos[N] = n
		if ( num < 1 )
			num = 0
		count++
	}

	// Salida
	out.innerHTML = "Del derecho <br>"
	for ( i = (count-1); i>=0; i-- )
		out.innerHTML += digitos[i] + " <br> "
	
	out.innerHTML += "<br>Del revés <br>"
	for ( i=0; i<count; i++ )
		out.innerHTML += digitos[i] + " <br> "
}